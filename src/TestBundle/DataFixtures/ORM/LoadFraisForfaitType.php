<?php

namespace TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\FraisForfaitType;

class LoadFraisForfaitType implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tab = array(
            array('libelle'=>'Nuitée Hôtel', 'montant'=>'80'),
            array('libelle'=>'Repas restaurant', 'montant'=>'25'),
            array('libelle'=>'Forfait kilométrique', 'montant'=>'0.62')
        );

        foreach($tab as $row) {
            $fraisForfaitType = new FraisForfaitType();
            $fraisForfaitType->setLibelle($row['libelle']);
            $fraisForfaitType->setMontant($row['montant']);

            $manager->persist($fraisForfaitType);
        }
        $manager->flush();
    }
}