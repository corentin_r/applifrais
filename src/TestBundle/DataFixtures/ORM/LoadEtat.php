<?php

namespace TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\Etat;

class LoadEtat implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $libelles = array(
            'Fiche créée, saisie en cours','Saisie cloturée','Validée'
        );

        foreach($libelles as $libelle) {
            $etat = new Etat();
            $etat->setLibelle($libelle);

            $manager->persist($etat);
        }
        $manager->flush();
    }
}