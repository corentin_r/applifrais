<?php

namespace TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\Visiteur;

class LoadVisiteur implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $date = new \DateTime();

        $tab = array(
            array(
                'nom' => 'Dupin',
                'prenom' => 'Jacques-Admin',
                'email' => 'yolo@email.com',
                'user' => 'admin',
                'password' => 'admin',
                'role' => '[ROLE_ADMIN]',
                'adresse' => '3 rue Dupin',
                'cp' => '69001',
                'ville' => 'Lyon',
            )
        );

        foreach($tab as $row) {
            $visiteur = new Visiteur();
            $visiteur->setEmail($row['email']);
            $visiteur->setNom($row['nom']);
            $visiteur->setPrenom($row['prenom']);
            $visiteur->setUsername($row['user']);
            $visiteur->setPlainPassword($row['password']);
            $visiteur->setRoles(array('ROLE_ADMIN'));
            $visiteur->setAdresse($row['adresse']);
            $visiteur->setCp($row['cp']);
            $visiteur->setVille($row['ville']);
            $visiteur->setEnabled(true);
            $visiteur->setDateEmbauche($date->setDate(2012, 4, 27));

            $manager->persist($visiteur);
        }
        $manager->flush();
    }
}