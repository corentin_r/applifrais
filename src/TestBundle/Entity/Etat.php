<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Etat
 *
 * @ORM\Table(name="etat")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=30)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="TestBundle\Entity\FraisForfait", mappedBy="etat")
     */
    private $fraisForfait;

    /**
     * @ORM\OneToMany(targetEntity="TestBundle\Entity\FraisHorsForfait", mappedBy="etat")
     */
    private $fraisHorsForfait;

    /**
     * @ORM\OneToMany(targetEntity="TestBundle\Entity\FicheFrais", mappedBy="etat")
     */
    private $fichesFrais;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Etat
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ficheFrais = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fraisForfait = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fraisHorsForfait = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add fraisForfait
     *
     * @param \TestBundle\Entity\FraisForfait $fraisForfait
     *
     * @return Etat
     */
    public function addFraisForfait(\TestBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfait[] = $fraisForfait;

        return $this;
    }

    /**
     * Remove fraisForfait
     *
     * @param \TestBundle\Entity\FraisForfait $fraisForfait
     */
    public function removeFraisForfait(\TestBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfait->removeElement($fraisForfait);
    }

    /**
     * Get fraisForfait
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisForfait()
    {
        return $this->fraisForfait;
    }

    /**
     * Add fraisHorsForfait
     *
     * @param \TestBundle\Entity\FraisHorsForfait $fraisHorsForfait
     *
     * @return Etat
     */
    public function addFraisHorsForfait(\TestBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfait[] = $fraisHorsForfait;

        return $this;
    }

    /**
     * Remove fraisHorsForfait
     *
     * @param \TestBundle\Entity\FraisHorsForfait $fraisHorsForfait
     */
    public function removeFraisHorsForfait(\TestBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfait->removeElement($fraisHorsForfait);
    }

    /**
     * Get fraisHorsForfait
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisHorsForfait()
    {
        return $this->fraisHorsForfait;
    }

    /**
     * Add ficheFrais
     *
     * @param \TestBundle\Entity\FicheFrais $ficheFrais
     *
     * @return EtatFiche
     */
    public function addFicheFrais(\TestBundle\Entity\FicheFrais $ficheFrais)
    {
        $this->fichesFrais[] = $ficheFrais;

        return $this;
    }

    /**
     * Remove ficheFrais
     *
     * @param \TestBundle\Entity\FicheFrais $ficheFrais
     */
    public function removeFicheFrais(\TestBundle\Entity\FicheFrais $ficheFrais)
    {
        $this->fichesFrais->removeElement($ficheFrais);
    }

    /**
     * Get ficheFrais
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFicheFrais()
    {
        return $this->ficheFrais;
    }

    /**
     * Add fichesFrai
     *
     * @param \TestBundle\Entity\FicheFrais $fichesFrai
     *
     * @return Etat
     */
    public function addFichesFrai(\TestBundle\Entity\FicheFrais $fichesFrai)
    {
        $this->fichesFrais[] = $fichesFrai;

        return $this;
    }

    /**
     * Remove fichesFrai
     *
     * @param \TestBundle\Entity\FicheFrais $fichesFrai
     */
    public function removeFichesFrai(\TestBundle\Entity\FicheFrais $fichesFrai)
    {
        $this->fichesFrais->removeElement($fichesFrai);
    }

    /**
     * Get fichesFrais
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFichesFrais()
    {
        return $this->fichesFrais;
    }
}
