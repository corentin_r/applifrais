<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FicheFrais
 *
 * @ORM\Table(name="fiche_frais")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\FicheFraisRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FicheFrais
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="mois", type="integer")
     */
    private $mois;

    /**
     * @var int
     *
     * @ORM\Column(name="annee", type="integer")
     */
    private $annee;

    /**
     * @var int
     *
     * @ORM\Column(name="nbJustificatifs", type="integer", nullable=true)
     */
    private $nbJustificatifs;

    /**
     * @var float
     *
     * @ORM\Column(name="montantValide", type="float", nullable=true)
     */
    private $montantValide;

    /**
     * @ORM\ManyToOne(targetEntity="TestBundle\Entity\Etat", inversedBy="ficheFrais", cascade={"persist"})
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="date")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModif", type="date")
     */
    private $dateModif;

    /**
     * @ORM\ManyToOne(targetEntity="TestBundle\Entity\Visiteur", inversedBy="fiches")
     *
     */
    private $visiteur;


    /**
     * @ORM\OneToMany(targetEntity="TestBundle\Entity\FraisForfait", mappedBy="Fiche", cascade={"persist"})
     */
    private $fraisForfaits;

    /**
     * @ORM\OneToMany(targetEntity="TestBundle\Entity\FraisHorsForfait", mappedBy="Fiche", cascade={"persist"})
     */
    private $fraisHorsForfaits;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idVisiteur
     *
     * @param string $idVisiteur
     *
     * @return FicheFrais
     */
    public function setIdVisiteur($idVisiteur)
    {
        $this->idVisiteur = $idVisiteur;

        return $this;
    }

    /**
     * Get idVisiteur
     *
     * @return string
     */
    public function getIdVisiteur()
    {
        return $this->idVisiteur;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return FicheFrais
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set nbJustificatifs
     *
     * @param integer $nbJustificatifs
     *
     * @return FicheFrais
     */
    public function setNbJustificatifs($nbJustificatifs)
    {
        $this->nbJustificatifs = $nbJustificatifs;

        return $this;
    }

    /**
     * Get nbJustificatifs
     *
     * @return int
     */
    public function getNbJustificatifs()
    {
        return $this->nbJustificatifs;
    }

    /**
     * Set montantValide
     *
     * @param float $montantValide
     *
     * @return FicheFrais
     */
    public function setMontantValide($montantValide)
    {
        $this->montantValide = $montantValide;

        return $this;
    }

    /**
     * Get montantValide
     *
     * @return float
     */
    public function getMontantValide()
    {
        return $this->montantValide;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     * @return FicheFrais
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }


    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }


    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set visiteur
     *
     * @param \TestBundle\Entity\Visiteur $visiteur
     *
     * @return FicheFrais
     */
    public function setVisiteur(\TestBundle\Entity\Visiteur $visiteur = null)
    {
        $this->visiteur = $visiteur;

        return $this;
    }

    /**
     * Get visiteur
     *
     * @return \TestBundle\Entity\Visiteur
     */
    public function getVisiteur()
    {
        return $this->visiteur;
    }


    /**
     * Set annee
     *
     * @param integer $annee
     *
     * @return FicheFrais
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    public function setDateCreation()
    {
        $this->dateCreation = new \DateTime();
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateDateCreation()
    {
        $this->dateCreation = new \DateTime();
        $this->dateModif = new \DateTime();

    }
    /**
     * @ORM\PreUpdate()
     */
    public function updateDateModif()
    {
        $this->dateModif = new \DateTime();

    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fraisForfaits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fraisHorsForfaits = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Add fraisForfait
     *
     * @param \TestBundle\Entity\FraisForfait $fraisForfait
     *
     * @return FicheFrais
     */
    public function addFraisForfait(\TestBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfaits[] = $fraisForfait;
        $fraisForfait->setFiche($this);
        return $this;
    }

    /**
     * Remove fraisForfait
     *
     * @param \TestBundle\Entity\FraisForfait $fraisForfait
     */
    public function removeFraisForfait(\TestBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfaits->removeElement($fraisForfait);
    }

    /**
     * Get fraisForfaits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisForfaits()
    {
        return $this->fraisForfaits;
    }

    /**
     * Add fraisHorsForfait
     *
     * @param \TestBundle\Entity\FraisHorsForfait $fraisHorsForfait
     *
     * @return FicheFrais
     */
    public function addFraisHorsForfait(\TestBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfaits[] = $fraisHorsForfait;
        $fraisHorsForfait->setFiche($this);
        return $this;
    }

    /**
     * Remove fraisHorsForfait
     *
     * @param \TestBundle\Entity\FraisHorsForfait $fraisHorsForfait
     */
    public function removeFraisHorsForfait(\TestBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfaits->removeElement($fraisHorsForfait);
    }

    /**
     * Get fraisHorsForfaits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisHorsForfaits()
    {
        return $this->fraisHorsForfaits;
    }

    /**
     * Set etat
     *
     * @param \TestBundle\Entity\Etat $etat
     *
     * @return FicheFrais
     */
    public function setEtat(\TestBundle\Entity\Etat $etat = null)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return \TestBundle\Entity\Etat
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
