<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisiteurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('nom')
            ->add('prenom')
            ->add('adresse')
            ->add('cp')
            ->add('ville')
            ->add('dateEmbauche')
            ->add('username')
            ->add('email')
            ->add('plainPassword')
            //->add('dateModif')
            //->add('dateCreation')
           /*->add('roles', CollectionType::class, array(
                   'choices' => array(
                    'ROLE_ADMIN' => 'Comptable',
                    'ROLE_SUPER_ADMIN' => 'Super Admin',
                    'ROLE_USER' => 'User')
               )
           )*/
             /*->add('role', ChoiceType::class, array(
                'choices' => array(
                    'ROLE_ADMIN' => 'Comptable',
                    'ROLE_SUPER_ADMIN' => 'Super Admin',
                    'ROLE_USER' => 'User')
                ))*/

            ->add('roles', ChoiceType::class, array(
                'multiple' => true,
                'choices' => array('ROLE_ADMIN' => 'ROLE_ADMIN', 'ROLE_USER' => 'ROLE_USER')))
            ->add("Ajoute", SubmitType::class)
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Visiteur'
        ));
    }
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    public function getBlockPrefix()
    {
        return 'testbundle_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
