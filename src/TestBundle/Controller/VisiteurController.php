<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Entity\Visiteur;

class VisiteurController extends Controller
{

    public function loginAction()
    {
        return $this->render('@Test/Default/login.html.twig',
            array('visiteur' => $visiteurs,
                ));
    }

    public function usersListAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('TestBundle:Visiteur')->findAll();

        return $this->render('@Test/Admin/liste-utilisateurs.html.twig',
            array('users' => $users));
    }

    public function addVisiteurAction(Request $request)
    {
        $user = new Visiteur();
        $form = $this->createForm('TestBundle\Form\VisiteurType', $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){ //vérifie si le form est valide
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user); //persist=enregistrement
            $em->flush(); //envoie dans la bdd et vide la mémoire
            $this->addFlash("success", "Utilisateur ajouté avec succès"); //affiche un message

        }

        //return this->$this->redirectToRoute('test_homepage'); //redirection vers la route du choix
        return $this->render("@Test/Admin/creer-visiteur.html.twig", //choix de la page de rendu
            array('form'=>$form->createView()));
    }


}