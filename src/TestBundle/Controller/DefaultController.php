<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Entity\FicheFrais;

class DefaultController extends Controller
{
    public function homePageAction(Request $request)
    {
        $user = $this->getUser();

        //Vérification de l'état des fiches
        $moisEnCours = (new \DateTime())->format('m');
        $anneeEnCours = (new \DateTime())->format('Y');
        $jourEnCours = (new \DateTime())->format('d');
        $etatCloturé = $this->getDoctrine()->getRepository('TestBundle:Etat')->find(2);

        $fichesFrais = $this->getDoctrine()->getRepository('TestBundle:FicheFrais')->findAll();
        foreach ($fichesFrais as $uneFiche){
            if ($uneFiche->getMois() <= $moisEnCours &&  $jourEnCours>10 && $uneFiche->getEtat()->getId()==1) {
                $uneFiche->setEtat($etatCloturé);

                $fraisForfaits = $uneFiche->getFraisForfaits();
                $fraisHorsForfaits = $uneFiche->getFraisHorsForfaits();

                //Modifie les frais pour qu'eux aussi soient cloturés
                foreach ($fraisForfaits as $fraisForfait){
                    $fraisForfait->setEtat($etatCloturé);
                }
                foreach ($fraisHorsForfaits as $fraisHorsForfait){
                    $fraisHorsForfait->setEtat($etatCloturé);
                }
                //Envoie les modifs en bdd
                $em = $this->getDoctrine()->getManager();
                $em->persist($uneFiche);
                $em->flush();
            }
        }


        //Renvoie la homepage correspondante au role de l'user
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY') || $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $roles = $user->getRoles();

            foreach ($roles as $role) {
                if ($role == "ROLE_ADMIN") {
                    return $this->render('@Test/Admin/homepage_admin.html.twig',
                        array('user'=> $user,
                            'roles' => $roles
                        ));
                } else {
                    return $this->render('@Test/Visiteur/homepage.html.twig',
                        array('user'=> $user,
                            'roles' => $roles
                        ));
                }
            }
        } else {
            return $this->render('@Test/Visiteur/homepage.html.twig',
                        array('user'=> $user
                        ));
        }
    }

    public function validerFicheAction(Request $request, $id)
    {
        $fichefrais = $this->getDoctrine()->getRepository('TestBundle:FicheFrais')->find($id);
        $fichefrais->setEtat();
        $em = $this->getDoctrine()->getManager();
        $em->persist($fichefrais);
        $em->flush();

        return $this->redirectToRoute('gerer-fichefrais');
    }

    public function siteMapAction(){
        $user = $this->getUser();
        $roles = $user->getRoles();

        return $this->render("@Test/Default/sitemap.html.twig",
            array ('user' => $user,
                'roles' => $roles
                ));
    }


}