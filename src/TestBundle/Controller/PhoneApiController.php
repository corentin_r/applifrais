<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class PhoneApiController extends Controller
{
    public function connectAction(Request $request)
    {

        $username= $request->get("username");
        $password= $request->get("password");
        $success = false;
        $message = "";


        $user = $this->getDoctrine()->getRepository('TestBundle:Visiteur')->findOneByUsername($username);


        if ($user) {
            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($user);
            $success = $encoder->isPasswordValid(
                $user->getPassword(),
                $password,
                $user->getSalt()
            );

            if ($success) {
                $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());

                $context = $this->get('security.token_storage');
                $context->setToken($token);

                $message = $user->getId();
            } else {
                $message = "Refusé";
            }
        }else {
            $message = "Utilisateur inconnu";
        }

        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success,
            'message' => $message
        ));

        return $response;
    }

    public function listeFichesAction(Request $request)
    {
        $id= $request->get("id");
        $user = $this->getDoctrine()->getRepository('TestBundle:Visiteur')->find($id);

        $liste_fiches = $user->getFiches();

        $success = false;
        $fiches  = array();

        if ($liste_fiches->count() > 0 ) {
            $success = true;

            foreach($liste_fiches as $row) {
                $uneFiche = array();
                $uneFiche["id"] = $row->getId();
                $uneFiche["mois"] = $row->getMois();
                $uneFiche["annee"] = $row->getAnnee();
                array_push($fiches, $uneFiche);
            }
        }

        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success,
            'fiches' => $fiches,
        ));

        return $response;
    }


    public function detailsFicheAction(Request $request)
    {
        $id = $request->get('id');
        $fichefrais = $this->getDoctrine()->getRepository('TestBundle:FicheFrais')->find($id);

        $fraisforfait = $fichefrais->getFraisForfaits();
        //$fraishorsforfait = $fichefrais->getFraisHorsForfaits();

        $success = false;
        $lignes  = "Aucune ligne frais trouvee";
        //$lignesHF  = "Aucune ligne frais hors forfait trouvee";

        if ($fraisforfait->count() > 0 ) {
            $success = true;
            $lignes = array();
            foreach($fraisforfait as $row) {
                $uneFiche["idFF"] = $row->getId();
                $uneFiche["libelleFF"] = $row->getFraisType()->getLibelle();
                $uneFiche["quantiteFF"] = $row->getQuantite();
                $uneFiche["prixFF"] = $row->getFraisType()->getMontant();
                $uneFiche["etatFF"] = $row->getEtat()->getLibelle();

                array_push($lignes, $uneFiche);
            }

        }

        /*if ($fraishorsforfait->count() > 0 ) {
            $success = true;
            $lignesHF = array();
            foreach($fraishorsforfait as $row) {
                $uneFicheHF["idHF"] = $row->getId();
                $uneFicheHF["libelleHF"] = $row->getLibelle();
                $uneFicheHF["quantiteHF"] = $row->getQuantite();
                $uneFicheHF["prixHF"] = $row->getMontant();

                array_push($lignesHF, $uneFicheHF);
            }

        }*/

        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success,
            'fraisFF' => $lignes,
            //'fraisHF' => $lignesHF
            //'fiches' => $lignes,
            //'ficheshf' => $lignesHF
        ));

        return $response;
    }
}
