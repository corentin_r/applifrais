<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FraisForfaitController extends Controller
{
    public function editFraisForfaitAction(Request $request, $id)
    {
        $referer = $request->headers->get('referer');
        $fraisforfait = $this->getDoctrine()->getRepository('TestBundle:FraisForfait')->find($id);
        $form = $this->createForm('TestBundle\Form\FraisForfaitType', $fraisforfait);
        $form->handleRequest($request);
        $fichefrais = $fraisforfait->getFiche()->getId();

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($fraisforfait);
            $em->flush();
            return $this->redirectToRoute('liste-fiches-frais');
        }

        return $this->render('@Test/Visiteur/modifier-frais.html.twig',
            array('form'=>$form->createView(),
                "frais" => $fraisforfait,
                "referer" => $referer,
            ));
    }


    public function removeFraisForfaitAction(Request $request, $id)
    {

        $fraisForfait = $this->getDoctrine()->getRepository('TestBundle:FraisForfait')->find($id);
        $fichefrais = $fraisForfait->getFiche()->getId();
        if ($fraisForfait != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisForfait);
            $em->flush();
        }
        return $this->redirectToRoute('liste-fiches-frais');
    }

    public function approveFraisForfaitAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $fraisforfait = $this->getDoctrine()->getRepository('TestBundle:FraisForfait')->find($id);
        $etatValidé = $em->getRepository("TestBundle:Etat")->find(3);

        $fraisforfait->setEtat($etatValidé);
        $em->persist($fraisforfait);
        $em->flush();
        $ficheId = $fraisforfait->getFiche()->getId();


        $this->addFlash("success", "Frais forfait validé");
        return $this->redirectToRoute('fichefrais_voir', array('id' => $ficheId));
    }

    public function disapproveFraisForfaitAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $fraisforfait = $this->getDoctrine()->getRepository('TestBundle:FraisForfait')->find($id);
        $etatInitial = $em->getRepository("TestBundle:Etat")->find(2);
        $fraisforfait->setEtat($etatInitial);
        $em->persist($fraisforfait);
        $em->flush();
        $ficheId = $fraisforfait->getFiche()->getId();

        $this->addFlash("success", "Frais forfait rétabli");
        return $this->redirectToRoute('fichefrais_voir', array('id' => $ficheId));
    }
}
