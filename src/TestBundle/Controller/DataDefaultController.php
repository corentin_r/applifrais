<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TestBundle\Entity\Etat;
use TestBundle\Entity\FraisForfaitType;
use TestBundle\Entity\Visiteur;


class DataDefaultController extends Controller
{
    public function createDataAction($name)
    {
        $em = $this->getDoctrine()->getManager();
        $date = new \DateTime();

        $etat = new Etat();
        $etat->setId('1');
        $etat->setLibelle('Fiche créée, saisie en cours');
        $em->persist($etat);
        $em->flush();

        $etat = new Etat();
        $etat->setId('2');
        $etat->setLibelle('Saisie cloturée');
        $em->persist($etat);
        $em->flush();

        $etat = new Etat();
        $etat->setId('3');
        $etat->setLibelle('Validée');
        $em->persist($etat);
        $em->flush();

        $forfaitType = new FraisForfaitType();
        $forfaitType->setLibelle('Nuitée Hôtel');
        $forfaitType->setMontant('80');
        $em->persist($forfaitType);
        $em->flush();

        $forfaitType = new FraisForfaitType();
        $forfaitType->setLibelle('Repas restaurant');
        $forfaitType->setMontant('25');
        $em->persist($forfaitType);
        $em->flush();

        $forfaitType = new FraisForfaitType();
        $forfaitType->setLibelle('Forfait kilométrique');
        $forfaitType->setMontant('0.62');
        $em->persist($forfaitType);
        $em->flush();

        $visiteur = new Visiteur();
        $visiteur->setNom('Dupin');
        $visiteur->setPrenom('Jacques-Admin');
        $visiteur->setUsername('admin');
        $visiteur->setPassword('admin');
        $visiteur->setRoles('ROLE_ADMIN');
        $visiteur->setAdresse('3 rue Dupin');
        $visiteur->setCp('69001');
        $visiteur->setVille('Lyon');
        $visiteur->setDateEmbauche($date->setDate(2012, 4, 27));
        $em->persist($visiteur);
        $em->flush();

        return new Response('<html><h5>Vous avez bien créé les états, les forfaits types et un admin (admin, admin)</h5></html>');
    }
}
