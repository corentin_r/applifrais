<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FraisHorsForfaitController extends Controller
{
    public function editFraisHorsForfaitAction(Request $request, $id)
    {
        $referer = $request->headers->get('referer');

        $fraishorsforfait = $this->getDoctrine()->getRepository('TestBundle:FraisHorsForfait')->find($id);
        $form = $this->createForm('TestBundle\Form\FraisHorsForfaitType', $fraishorsforfait);
        $form->handleRequest($request);
        $fichefrais = $fraishorsforfait->getFiche()->getId();

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($fraishorsforfait);
            $em->flush();
            return $this->redirectToRoute('liste-fiches-frais');

        }

        return $this->render('@Test/Visiteur/modifier-frais.html.twig',
            array('form'=>$form->createView(),
                "frais" => $fraishorsforfait,
                "referer" => $referer,
            ));
    }

    public function removeFraisHorsForfaitAction(Request $request, $id)
    {

        $fraisHorsforfait = $this->getDoctrine()->getRepository('TestBundle:FraisHorsForfait')->find($id);
        $fichefrais = $fraisHorsforfait->getFiche()->getId();
        if ($fraisHorsforfait != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisHorsforfait);
            $em->flush();
        }
        return $this->redirectToRoute('liste-fiches-frais');
    }

    public function approveFraisHorsForfaitAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $fraishorsforfait = $this->getDoctrine()->getRepository('TestBundle:FraisHorsForfait')->find($id);
        $etatValidé = $em->getRepository("TestBundle:Etat")->find(3);
        $fraishorsforfait->setEtat($etatValidé);

        $em->persist($fraishorsforfait);
        $em->flush();
        $ficheId = $fraishorsforfait->getFiche()->getId();



        $this->addFlash("success", "Frais hors forfait validé");
        return $this->redirectToRoute('fichefrais_voir', array('id' => $ficheId));
    }

    public function disapproveFraisHorsForfaitAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $fraishorsforfait = $this->getDoctrine()->getRepository('TestBundle:FraisHorsForfait')->find($id);
        $etatInitial = $em->getRepository("TestBundle:Etat")->find(2);
        $fraishorsforfait->setEtat($etatInitial);
        $em->persist($fraishorsforfait);
        $em->flush();
        $ficheId = $fraishorsforfait->getFiche()->getId();

        $this->addFlash("success", "Frais hors forfait rétabli");
        return $this->redirectToRoute('fichefrais_voir', array('id' => $ficheId));
    }
}
